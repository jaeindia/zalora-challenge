'''
Created on Dec 13, 2015

@author: Jayakumar
'''

def list_of_list(input_array):
    for obj in input_array:
        if isinstance(obj, list):
            list_of_list(obj)
        else:
            print obj


# Main
input_array = [['a', 'b', 'c'], ['d', 'e', 'f']]
list_of_list(input_array)