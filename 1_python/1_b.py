'''
Created on Dec 13, 2015

@author: Jayakumar
'''

import csv
import collections

def determine_brand_popularity(file_name):
    brand_dict = {}
    
    with open (file_name, "r") as input_file:
        reader = csv.reader(input_file)
    
        for row in reader:
            for bucket in row:
                bucket = bucket.translate(None, "[]")
                bucket = bucket.split(',')
                for brand in bucket:
                    brand_dict[brand] = brand_dict.get(brand, 0) + 1
    
    # Sort by count, brand name desc
    return collections.OrderedDict(sorted(brand_dict.items(), key=lambda value:(value[1], value[0]), reverse=True))
    
        
# Main
popular_brand = determine_brand_popularity("../Input/top10_sample.csv")
for brand, count in popular_brand.iteritems():
        print "%s: %s" % (brand, count)
