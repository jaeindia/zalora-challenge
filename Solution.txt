=============================================================================
1. Python / Haskell / Perl / Your prefered scripting language.
a) array = [['a','b','c'],['d','e','f']]
How do I get this output?
a
b
c
d
e
f

'''
Created on Dec 13, 2015

@author: Jayakumar
'''

def list_of_list(input_array):
    for obj in input_array:
        if isinstance(obj, list):
            list_of_list(obj)
        else:
            print obj


# Main
input_array = [['a', 'b', 'c'], ['d', 'e', 'f']]
list_of_list(input_array)


b) Have a look at "programming-tasks/top10_sample.csv"
Each line in this file represents a list of Brand in our store.
Write a script to print out a list of brand names and their occurrence counts (sorted).

'''
Created on Dec 13, 2015

@author: Jayakumar
'''

import csv
import collections

def determine_brand_popularity(file_name):
    brand_dict = {}
    
    with open (file_name, "r") as input_file:
        reader = csv.reader(input_file)
    
        for row in reader:
            for bucket in row:
                bucket = bucket.translate(None, "[]")
                bucket = bucket.split(',')
                for brand in bucket:
                    brand_dict[brand] = brand_dict.get(brand, 0) + 1
    
    # Sort by count, brand name desc
    return collections.OrderedDict(sorted(brand_dict.items(), key=lambda value:(value[1], value[0]), reverse=True))
    
        
# Main
popular_brand = determine_brand_popularity("../Input/top10_sample.csv")
for brand, count in popular_brand.iteritems():
        print "%s: %s" % (brand, count)

=============================================================================
2. SQL
a) What is the relation between Database, Schema, Tables, View in PostgreSQL / MySQL ?

Database: Collection of organised data and specific features to access them. Organised means in the form of tables, views and stored procedures, functions etc to access the data.

Schema: It is a framework or kind of concept to help organizing and interpreting data. In short term, it is a kind of short cuts to reach vast amount of database on rules defined in the schema.

Table: Collection of Rows and Columns to store the data. Columns are the name of the fields, rows are the actual data.

Views: It is the result set of a stored query on the data. A view does not form part of the physical schema, as a result set, it is a virtual table computed or collated dynamically from data in the database when access to that view is requested.

b) What is the difference between a table and a view ?

A table contains data, a view is just a SELECT statement which has been saved in the database (more or less, depending on your database).

The advantage of a view is that it can join data from several tables, thus creating a new view of it. Say you have a database with salaries and you need to do some complex statistical queries on it. Instead of sending the complex query to the database all the time, you can save the query as a view and then 

SELECT *
FROM < view_name >

c) Table reporting.items has 4 columns: Item_Code - Date - Visits - Orders
Write a query to get total number of Visit over all Item_Codes for the day '2013-01-12'.

SELECT COUNT(item.Visits) AS "Total number of Visits"
FROM reporting.items AS item
WHERE TO_CHAR(item.DATE, 'YYYY-MM-DD') = '2013-01-12';

d) As a DBA: in PostgreSQL DB, write query(s) needed to give account "buying" access to all tables currently in schema "sales", and all future Tables created in schema "sales".


-- Give privileges for the already existing tables
REVOKE ALL
	ON ALL TABLES IN SCHEMA sales
	FROM PUBLIC;

GRANT SELECT,
	INSERT,
	UPDATE,
	DELETE
	ON ALL TABLES IN SCHEMA sales
	TO buying;

-- To achieve the same for new tables
ALTER DEFAULT PRIVILEGES
FOR USER buying IN SCHEMA sales

GRANT SELECT,
	INSERT,
	UPDATE,
	DELETE
	ON TABLES
	TO buying;

=============================================================================
3. Bash scripting
a) Write a bash script for the below set of tasks:
[
- connect to ftp server (host=10.11.12.13 port=22 username=helloFTP password=world)
- download all files that have their name started with "webtrekk_marketing" into "home/Marketing Report/Data/"
- run ZMR.py which is located in "home/Marketing Report/Scripts/"
- run UpdateWebtrekk.sql which is located in "home/Marketing Report/Scripts/" on a PostgreSQL DB (host=10.11.12.13 port=5439 database=zalora username=helloDB password=world)
]
How would you schedule the above as a cron job every day at 2.35am?

#!/bin/sh

$HOST='10.11.12.13'
$USER='helloFTP'
$PASSWD='world'

# - Connect to ftp server (host=10.11.12.13 port=22 username=helloFTP password=world)
ftp -in $HOST << EOF
user $USER $PASSWD

# - Download all files that have their name started with "webtrekk_marketing" into "home/Marketing Report/Data/"
cd home/Marketing Report/Data/
mget webtrekk_marketing*

# - Run ZMR.py which is located in "home/Marketing Report/Scripts/"
cd
cd home/Marketing Report/Scripts/
python ZMR.py

# - Run UpdateWebtrekk.sql which is located in "home/Marketing Report/Scripts/" on a PostgreSQL DB (host=10.11.12.13 port=5439 database=zalora username=helloDB password=world)
psql -h $HOST -d zalora -U helloDB -p 5439 -a -w -f UpdateWebtrekk.sql

# - How would you schedule the above as a cron job every day at 2.35am ?
# crontab - e
# 35 2 * * * /path/to/3_a.sh

EOF

b) Have a look at the folder "/programming-tasks/bash/"
- Write a bash script to rename all files below from "zalora-*" to "Zalora-*"
- All Zalora-* files contain a single string: "this is a test." (with a new line at the end):
    Write a shell script to change the content of those files to all uppercase.
    Write a shell script to remove all dot character (.) within those files.

#!/bin/sh

cd ../bash

# - Rename all files below from "zalora-*" to "Zalora-*"
for zal_file in zalora-*
do
    mv -i "$zal_file" "${zal_file/z/Z}"
done

# - Change the content of the files to uppercase.
for Zal_file in Zalora-*
do
    tr '[:lower:]' '[:upper:]' < $Zal_file > tmp
    mv tmp $Zal_file
done

# - Remove all dot character (.) within the files.
for Zal_file in Zalora-*
do
    sed 's/\.//g' < $Zal_file > tmp
    mv tmp $Zal_file 
done