#!/bin/sh

cd ../bash

# - Rename all files below from "zalora-*" to "Zalora-*"
for zal_file in zalora-*
do
    mv -i "$zal_file" "${zal_file/z/Z}"
done

# - Change the content of the files to uppercase.
for Zal_file in Zalora-*
do
    tr '[:lower:]' '[:upper:]' < $Zal_file > tmp
    mv tmp $Zal_file
done

# - Remove all dot character (.) within the files.
for Zal_file in Zalora-*
do
    sed 's/\.//g' < $Zal_file > tmp
    mv tmp $Zal_file 
done