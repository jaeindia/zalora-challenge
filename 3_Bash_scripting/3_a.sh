#!/bin/sh

$HOST='10.11.12.13'
$USER='helloFTP'
$PASSWD='world'

# - Connect to ftp server (host=10.11.12.13 port=22 username=helloFTP password=world)
ftp -in $HOST << EOF
user $USER $PASSWD

# - Download all files that have their name started with "webtrekk_marketing" into "home/Marketing Report/Data/"
cd home/Marketing Report/Data/
mget webtrekk_marketing*

# - Run ZMR.py which is located in "home/Marketing Report/Scripts/"
cd
cd home/Marketing Report/Scripts/
python ZMR.py

# - Run UpdateWebtrekk.sql which is located in "home/Marketing Report/Scripts/" on a PostgreSQL DB (host=10.11.12.13 port=5439 database=zalora username=helloDB password=world)
psql -h $HOST -d zalora -U helloDB -p 5439 -a -w -f UpdateWebtrekk.sql

# - How would you schedule the above as a cron job every day at 2.35am ?
# crontab - e
# 35 2 * * * /path/to/3_a.sh

EOF